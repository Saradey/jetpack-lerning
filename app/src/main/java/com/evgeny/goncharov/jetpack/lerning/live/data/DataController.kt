package com.evgeny.goncharov.jetpack.lerning.live.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class DataController {

    private val data: MyLiveData by lazy {
        MyLiveData()
    }

    val mutableLiveData: MutableLiveData<String> = MutableLiveData()


    companion object {
        val dataController: DataController by lazy {
            DataController()
        }
    }


    fun createLiveData(): MyLiveData {
        return data
    }


}