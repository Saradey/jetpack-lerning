package com.evgeny.goncharov.jetpack.lerning

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.evgeny.goncharov.jetpack.lerning.live.data.DataController
import kotlinx.android.synthetic.main.activity_main.*
import androidx.arch.core.util.Function
import androidx.lifecycle.*
import com.evgeny.goncharov.jetpack.lerning.view.model.MainViewModel
import io.reactivex.Observable

class MainActivity : AppCompatActivity() {


    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //LiveData - хранилище данных, работающее по принципу паттерна Observer (наблюдатель).
        // Это хранилище умеет делать две вещи:
        //
        //1) В него можно поместить какой-либо объект
        //
        //2) На него можно подписаться и получать объекты, которые в него помещают.

//        val liveData = DataController.dataController.createLiveData()
//
//        liveData.observe(this, Observer { data ->
//            txvMainTextView.text = data
//        })
//
//
//        btnSetValue.setOnClickListener {
//            liveData.setMyValue("Проверка")
//        }

//        val mutableLiveData = DataController.dataController.mutableLiveData
//
//        mutableLiveData.observe(this, Observer { data ->
//            txvMainTextView.text = data
//        })
//
//        btnSetValue.setOnClickListener {
//            mutableLiveData.postValue("Проверка")
//        }


        /////продвинутые возможности LiveData, мапнуть дату
//        val liveData = DataController.dataController.createLiveData()
//        val liveDataInt = Transformations.map(liveData) {
//            it.toInt()
//        }
//
//        btnSetValue.setOnClickListener {
//            liveData.setMyValue("10")
//        }
//
//        liveDataInt.observe(this, Observer { data ->
//            txvMainTextView.text = data.toString()
//        })


//        val liveData = DataController.dataController.createLiveData()
//        val liveDataInt = Transformations.switchMap(liveData) {
//            val live = MutableLiveData<Int>()
//            live.postValue(it.toInt())
//            live
//        }
//        btnSetValue.setOnClickListener {
//            liveData.setMyValue("10")
//        }
//        liveDataInt.observe(this, Observer { data ->
//            txvMainTextView.text = data.toString()
//        })


        ///////////////////////////////ViewModel
        //Соответственно, при поворотах экрана, Activity будет пересоздаваться, а объект
        // MainViewModel будет спокойно себе жить в провайдере.
        //Не храните в ViewModel ссылки на Activity, фрагменты, View и пр.
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        //У метода get, который возвращает нам модель из провайдера, есть еще такой вариант вызова:
        //T get (String key, Class<T> modelClass)


    }


}
