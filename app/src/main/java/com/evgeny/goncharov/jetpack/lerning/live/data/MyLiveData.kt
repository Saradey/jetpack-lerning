package com.evgeny.goncharov.jetpack.lerning.live.data

import androidx.lifecycle.LiveData

class MyLiveData : LiveData<String>() {


    fun setMyValue(value: String) {
        postValue(value)
    }

}